import React from "react";
import "../assets/css/controlpanel.css";
import {
  CheckBoxOutlineBlank,
  RadioButtonUnchecked,
  InsertDriveFile,
  DateRange,
  AddCircleOutline,
  TextFields,
  ArrowDropDown,
} from "@mui/icons-material";

const DraggableControl = ({ type }) => {
  const handleDragStart = (e) => {
    e.dataTransfer.setData("text/plain", type);
  };

  const iconMap = {
    "Checkbox Group": <CheckBoxOutlineBlank />,
    "Button": <AddCircleOutline />,
    "Date Field": <DateRange />,
    "Number": <TextFields />,
    "Radio Group": <RadioButtonUnchecked />,
    "file Upload": <InsertDriveFile />,
    "Select": <ArrowDropDown />,
    "Textarea": <TextFields />,
  };

  return (
    <div
      draggable
      onDragStart={handleDragStart}
      className="draggable-control"
      
    >
      <div className="control-icon">{iconMap[type]}</div>
      <div className="control-label">{type}</div>
    </div>
  );
};

export default DraggableControl;
