import React, { useState } from "react";
import "../assets/css/controlpanel.css";
import { Delete, Edit } from "@mui/icons-material";
import { ControlTypes } from "./FormBuilder";

const DraggableField = ({
  type,
  removeField,
  isRequired,
  editMode,
  updateFormData,
}) => {
  const [editing, setEditing] = useState(false);
  const [label, setLabel] = useState(type);
  const [name, setName] = useState("fieldName");
  const [placeholder, setPlaceholder] = useState("Placeholder");
  const [hovered, setHovered] = useState(false);
  const [isRequiredField, setIsRequiredField] = useState(isRequired);
  const [options, setOptions] = useState(["Option 1", "Option 2"]);
  const [newOption, setNewOption] = useState("");
  const [maxLength, setMaxLength] = useState(5);
  const [rows, setRows] = useState(3);
  const [showConfirmation, setShowConfirmation] = useState(false);

  const addOption = (e) => {
    e.preventDefault();
    if (newOption.trim() === "") return;
    setOptions([...options, newOption]);
    setNewOption("");
  };

  const removeOption = (index) => {
    const newOptions = [...options];
    newOptions.splice(index, 1);
    setOptions(newOptions);
  };

  const handleEdit = () => {
    setEditing(true);
  };

  const handleSave = () => {
    setEditing(false);
  };

  const handleCancel = () => {
    setEditing(false);
  };

  const handleToggleRequired = () => {
    setIsRequiredField(!isRequiredField);
  };

  const handleMouseEnter = () => {
    setHovered(true);
  };

  const handleMouseLeave = () => {
    setHovered(false);
  };

  const handleMaxLengthChange = (e) => {
    setMaxLength(parseInt(e.target.value) || 0);
  };

  const handleRowsChange = (e) => {
    setRows(parseInt(e.target.value) || 0);
  };

  const handleClear = () => {
    setShowConfirmation(true);
  };

  const handleConfirmationYes = () => {
    setOptions([]);
    setEditing(false);
    setShowConfirmation(false);
    removeField();
  };

  const handleConfirmationNo = () => {
    setShowConfirmation(false);
  };

  const handleFieldChange = (fieldName, updatedValue) => {
    updateFormData(fieldName, updatedValue);
  };

  const renderField = () => {
    if (editing) {
      return (
        <div className="edit-field">
          <div className="required-check">
            <label>Required:</label>
            <input
              type="checkbox"
              checked={isRequiredField}
              onChange={handleToggleRequired}
            />
          </div>
          <div>
            <label>
              Label:
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <input
              type="text"
              value={label}
              onChange={(e) => {
                const updatedValue = e.target.value;
                setLabel(updatedValue);
                handleFieldChange("label", updatedValue);
              }}
            />
          </div>
          <div>
            <label>
              Name:
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <input
              type="text"
              value={name}
              onChange={(e) => {
                const updatedValue = e.target.value;
                setName(updatedValue);
                handleFieldChange("name", updatedValue);
              }}
            />
          </div>
          <div>
            <label>
              Placeholder:
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <input
              type="text"
              value={placeholder}
              onChange={(e) => {
                const updatedValue = e.target.value;
                setPlaceholder(updatedValue);
                handleFieldChange("placeholder", updatedValue);
              }}
            />
          </div>
          {(type === ControlTypes["Checkbox Group"] ||
            type === ControlTypes["Radio Group"] ||
            type === ControlTypes["Select"]) && (
            <div>
              <label>Options:</label>
              {options.map((option, index) => (
                <div key={index} className="option-row">
                  <input
                    type={
                      type === ControlTypes["Checkbox Group"]
                        ? "checkbox"
                        : type === ControlTypes["Radio Group"]
                        ? "radio"
                        : type === ControlTypes["Select"]
                        ? "select"
                        : "text"
                    }
                  />
                  <input
                    type="text"
                    value={option}
                    onChange={(e) => {
                      const updatedValue = e.target.value;
                      const newOptions = [...options];
                      newOptions[index] = updatedValue;
                      setOptions(newOptions);
                      handleFieldChange("options", newOptions);
                    }}
                  />
                  <span
                    className="remove-option"
                    onClick={() => removeOption(index)}
                  >
                    Remove
                  </span>
                </div>
              ))}
              <div>
                <input
                  type="text"
                  placeholder="Add Option"
                  value={newOption}
                  onChange={(e) => setNewOption(e.target.value)}
                />
                <button className="add-option" onClick={(e) => addOption(e)}>
                  Add Option
                </button>
              </div>
            </div>
          )}
          {type === ControlTypes["Textarea"] && (
            <div>
              <div>
                <label>
                  Maximum Length:
                  {isRequiredField && <span className="required-label">*</span>}
                </label>
                <input
                  type="number"
                  value={maxLength}
                  onChange={handleMaxLengthChange}
                />
              </div>
              <div>
                <label>
                  Rows:
                  {isRequiredField && <span className="required-label">*</span>}
                </label>
                <input type="number" value={rows} onChange={handleRowsChange} />
              </div>
            </div>
          )}
          <div className="edit-icons">
            <button className="save-button" onClick={handleSave}>
              Save
            </button>
            <button className="cancel-button" onClick={handleCancel}>
              Cancel
            </button>
          </div>
        </div>
      );
    }

    switch (type) {
      case ControlTypes["Checkbox Group"]:
        return (
          <div className="field-functionality">
            <label className="field-label">
              {label}{" "}
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            {options.map((option, index) => (
              <div key={index}>
                <input type="checkbox" />
                <label>{option}</label>
              </div>
            ))}
          </div>
        );
      case ControlTypes["Button"]:
        return (
          <div className="field-functionality">
            <button type="button" className="actual-button">
              {label}
            </button>
            {isRequiredField && <span className="required-label">*</span>}
          </div>
        );
      case ControlTypes["Date Field"]:
        return (
          <div className="field-functionality">
            <label className="field-label">
              {label}{" "}
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <div>
              <input type="date" />
            </div>
          </div>
        );
      case ControlTypes["Number"]:
        return (
          <div className="field-functionality">
            <label className="field-label">
              {label}{" "}
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <div>
              <input type="number" />
            </div>
          </div>
        );
      case ControlTypes["Radio Group"]:
        return (
          <div className="field-functionality">
            <label className="field-label">
              {label}{" "}
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            {options.map((option, index) => (
              <div key={index}>
                <input type="radio" id={`option${index}`} name="radioGroup" />
                <label htmlFor={`option${index}`}>{option}</label>
              </div>
            ))}
          </div>
        );
      case ControlTypes["file Upload"]:
        return (
          <div className="field-functionality">
            <label className="field-label">
              {label}{" "}
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <div>
              <input type="file" />
            </div>
          </div>
        );
      case ControlTypes["Select"]:
        return (
          <div className="field-functionality">
            <label className="field-label">
              {label}{" "}
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <div>
              <select>
                {options.map((option, index) => (
                  <option key={index} value={option}>
                    {option}
                  </option>
                ))}
              </select>
            </div>
          </div>
        );
      case ControlTypes["Textarea"]:
        return (
          <div className="field-functionality">
            <label className="field-label">
              {label}{" "}
              {isRequiredField && <span className="required-label">*</span>}
            </label>
            <div>
              <textarea
                placeholder={placeholder}
                rows={rows}
                cols="100"
                maxLength={maxLength}
              ></textarea>
            </div>
          </div>
        );
      default:
        return null;
    }
  };

  return (
    <div
      className={`form-field ${editMode ? "hoverable" : ""} `}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      style={{ border: editMode ? "1px solid #ccc" : "none" }}
    >
      {editMode && hovered && (
        <div className="edit-icons">
          <span className="edit-field-icon" onClick={handleEdit}>
            <Edit />
          </span>
          <span className="remove-field" onClick={handleClear}>
            <Delete />
          </span>
        </div>
      )}
      {renderField()}
      {showConfirmation && (
        <div className="confirmation-dialog">
          <p>Are you sure you want to delete this field?</p>
          <div className="confirmation-buttons">
            <button
              className="confirmation-button yes"
              onClick={handleConfirmationYes}
            >
              Yes
            </button>
            <button
              className="confirmation-button no"
              onClick={handleConfirmationNo}
            >
              No
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default DraggableField;
