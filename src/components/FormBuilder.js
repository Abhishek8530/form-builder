import React, { useState, useRef } from "react";
import "../assets/css/controlpanel.css";
import DraggableControl from "./DraggableControl";
import DraggableField from "./DraggableField";

export const ControlTypes = {
  "Checkbox Group": "Checkbox Group",
  "Button": "Button",
  "Date Field": "Date Field",
  "Number": "Number",
  "Radio Group": "Radio Group",
  "file Upload": "file Upload",
  "Select": "Select",
  "Textarea": "Textarea",
};

const FormBuilder = () => {
  const [formFields, setFormFields] = useState([]);
  const [editMode, setEditMode] = useState(true);
  const [showClearConfirmation, setShowClearConfirmation] = useState(false);
  const [isFormSaved, setIsFormSaved] = useState(false);
  const [formData, setFormData] = useState({});
  const formDrop = useRef(null);

  const handleDrop = (e) => {
    e.preventDefault();
    const controlType = e.dataTransfer.getData("text/plain");
    const newFormFields = [...formFields, controlType];
    setFormFields(newFormFields);
  };

  const removeField = (index) => {
    const newFormFields = [...formFields];
    newFormFields.splice(index, 1);
    setFormFields(newFormFields);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setEditMode(false);
    setIsFormSaved(true);
  };

  const handleFormSubmit = () => {
    console.log("Form Data:", formData);
    alert("Form Data submitted: " + JSON.stringify(formData));
  };

  const toggleEditMode = () => {
    setEditMode(!editMode);
  };

  const handleClear = () => {
    setShowClearConfirmation(true);
  };

  const handleClearConfirmationYes = () => {
    setFormFields([]);
    setShowClearConfirmation(false);
    setEditMode(true);
    setIsFormSaved(false);
  };

  const handleClearConfirmationNo = () => {
    setShowClearConfirmation(false);
  };

  const updateFormData = (fieldName, value) => {
    setFormData({ ...formData, [fieldName]: value });
  };

  return (
    <div className="form-builder">
      <div className={`control-panel ${editMode ? "visible" : "hidden"}`}>
        {editMode
          ? Object.values(ControlTypes).map((type) => (
              <DraggableControl key={type} type={type} />
            ))
          : null}
      </div>
      <div
        className="form-container"
        ref={formDrop}
        onDrop={handleDrop}
        onDragOver={(e) => e.preventDefault()}
      >
        <form onSubmit={handleSubmit}>
          <div className="form">
            {formFields.map((fieldType, index) => (
              <DraggableField
                key={index}
                type={fieldType}
                removeField={() => removeField(index)}
                isRequired={false}
                editMode={editMode}
                updateFormData={updateFormData}
              />
            ))}
          </div>
          <div className="form-actions">
            {!isFormSaved ? (
              <button
                type="submit"
                className={`save-button ${editMode ? "visible" : "hidden"}`}
              >
                Save Form
              </button>
            ) : (
              <button
                type="button"
                className={`edit-button ${!editMode ? "visible" : "hidden"}`}
                onClick={toggleEditMode}
              >
                Edit
              </button>
            )}

            <button
              type="button"
              className="clear-button"
              onClick={handleClear}
            >
              Clear
            </button>
            {isFormSaved && (
              <button
                type="button"
                className={`submit-button ${editMode ? "visible" : "hidden"}`}
                onClick={handleFormSubmit}
              >
                Submit
              </button>
            )}
          </div>
        </form>
      </div>
      {showClearConfirmation && (
        <div className="clear-confirmation-dialog">
          <p>Are you sure you want to clear the form?</p>
          <div className="clear-confirmation-buttons">
            <button
              className="clear-confirmation-yes"
              onClick={handleClearConfirmationYes}
            >
              Yes
            </button>
            <button
              className="clear-confirmation-no"
              onClick={handleClearConfirmationNo}
            >
              No
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default FormBuilder;
