import React from 'react';
import FormBuilder from './components/FormBuilder';
import './App.css';

function App() {
  return (
    <div className="App">
      <FormBuilder />
    </div>
  );
}

export default App;
